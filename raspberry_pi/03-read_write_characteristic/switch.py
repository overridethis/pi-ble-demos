from sense_hat import SenseHat

hat = SenseHat()

class Switch():
    def __init__(self):
        self._color = (200,200,200)

    def get_color(self):
        return self._color

    def set_color(self, r, g, b):
        self._color = (r,g,b)
        self._update_led(self.get_status())

    def get_status(self):
        pixel_0 = hat.get_pixel(0,0)
        if (pixel_0[0] == 0 and pixel_0[1] == 0 and pixel_0[2] == 0):
            return 'off'
        return 'on'

    def set_status(self, status):
        self._update_led(status)

    def _update_led(self, status):
        if (status.lower() == 'on'):
            hat.set_pixels([self._color] * 64)
        else:
            hat.clear()