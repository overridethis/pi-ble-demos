#!/usr/bin/python3
from pybleno import Characteristic
from sense_hat import SenseHat

class SwitchCharacteristic(Characteristic):
    def __init__(self, uuid, switch):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['read','write'],
            'value': None
        })
        self._switch = switch

    def onReadRequest(self, offset, callback):
        value = self._switch.get_status().encode('UTF8')
        callback(Characteristic.RESULT_SUCCESS, value)

    def onWriteRequest(self, data, offset, withoutResponse, callback):
        status = data.decode('UTF8').lower()
        self._switch.set_status(status)
        callback(Characteristic.RESULT_SUCCESS)
