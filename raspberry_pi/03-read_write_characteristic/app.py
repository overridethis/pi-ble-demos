#!/usr/bin/python3
from pybleno import *
import sys
import os
import signal

from switch import Switch
from switch_characteristic import SwitchCharacteristic
from color_characteristic import ColorCharacteristic

switch = Switch()
bleno = Bleno()
uuid = 'ec01'

def onStateChange(state):
    print('on -> stateChange: ' + state)
    if (state == 'poweredOn'):
        advName = '{}-svc'.format(os.uname()[1])
        bleno.startAdvertising(advName,[uuid])
    else:
        bleno.stopAdvertising()

bleno.on('stateChange', onStateChange)

def on_advertisement_start(error):
    print('on -> advertisingStart: ' + ('error ' + error if error else 'success'))
    if not error:
        bleno.setServices([
            BlenoPrimaryService({
                'uuid': uuid,
                'characteristics': [
                SwitchCharacteristic('ec02', switch),
                ColorCharacteristic('ec03', switch)
            ]})
        ])
bleno.on('advertisingStart', on_advertisement_start)

bleno.start()

print ('Hit <ENTER> to disconnect\n')
if (sys.version_info > (3, 0)):
    input()
else:
    raw_input()

bleno.stopAdvertising()
bleno.disconnect()

print ('terminated.')
sys.exit(1)
