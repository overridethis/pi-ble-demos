#!/usr/bin/python3
from pybleno import Characteristic
import json

class ColorCharacteristic(Characteristic):
    def __init__(self, uuid, switch):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['read','write'],
            'value': None
        })

        self._switch = switch

    def onReadRequest(self, offset, callback):
        j = json.dumps(self._switch.get_color())
        value = j.encode('UTF-8')
        callback(Characteristic.RESULT_SUCCESS, value)

    def onWriteRequest(self, data, offset, withoutResponse, callback):
        j = json.loads(data.decode('UTF-8'))
        self._switch.set_color(j[0], j[1], j[2])
        callback(Characteristic.RESULT_SUCCESS)
