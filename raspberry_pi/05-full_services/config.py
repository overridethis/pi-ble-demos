# Service ID.
SERVICE_ID         = 'ec030000-0000-0000-0000-000000000000'
# Characteristic ID.
ENV_COMMAND_ID     = 'ec060000-0000-0000-0000-000000000000'
SWITCH_COMMAND_ID  = 'ec040000-0000-0000-0000-000000000000'
COLOR_COMMAND_ID   = 'ec050000-0000-0000-0000-000000000000'

class CharConfig:
    def __init__(self):
        self.env_command = ENV_COMMAND_ID.replace('-','').lower()
        self.switch_command = SWITCH_COMMAND_ID.replace('-','').lower()
        self.color_command = COLOR_COMMAND_ID.replace('-','').lower()

class Config:
    service_id = SERVICE_ID.replace('-','').lower()
    chararacteritics = CharConfig()