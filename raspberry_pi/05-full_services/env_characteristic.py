#!/usr/bin/python3
from pybleno import Characteristic
import json
from threading import Thread
import time

class EnvCharacteristic(Characteristic):
    def __init__(self, uuid, env):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['read','notify'],
            'value': None
        })
        self._env = env
        self._updateValueCallback = None

        # Notify every 30s
        def runNotifications():
            while True:
                time.sleep(30)
                if (self._updateValueCallback):
                    self._updateValueCallback(self._get_payload())

        Thread(target=runNotifications).start()

    def onReadRequest(self, offset, callback):
        callback(Characteristic.RESULT_SUCCESS, self._get_payload())

    def onSubscribe(self, maxValueSize, updateValueCallback):
        print('EnvCharacteristics - onSubscribe')
        self._updateValueCallback = updateValueCallback

    def onUnsubscribe(self):
        print('EnvCharacteristics - onUnsubscribe')
        self._updateValueCallback = None

    def _get_payload(self):
        data = self._env.get_data()
        payload = json.dumps(data).encode('UTF-8')
        return payload