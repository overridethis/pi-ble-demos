#!/usr/bin/python3
from pybleno import *
import sys
import os
import signal

from env_characteristic import EnvCharacteristic
from switch_characteristic import SwitchCharacteristic
from color_characteristic import ColorCharacteristic

from env_sensor import EnvSensor
from switch import Switch
from config import Config

bleno = Bleno()
uuid = Config.service_id

env_sensor = EnvSensor()
switch = Switch()

def onStateChange(state):
    print('on -> stateChange: ' + state)
    if (state == 'poweredOn'):
        advName = '{}-svc'.format(os.uname()[1])
        bleno.startAdvertising(advName,[uuid])
    else:
        bleno.stopAdvertising()

bleno.on('stateChange', onStateChange)

def on_advertisement_start(error):
    print('on -> advertisingStart: ' + ('error ' + error if error else 'success'))
    if not error:
        bleno.setServices([
            BlenoPrimaryService({
                'uuid': uuid,
                'characteristics': [
                    SwitchCharacteristic(Config.chararacteritics.switch_command, switch),
                    ColorCharacteristic(Config.chararacteritics.color_command, switch),
                    EnvCharacteristic(Config.chararacteritics.env_command, env_sensor)
                ]})
        ])
bleno.on('advertisingStart', on_advertisement_start)

bleno.start()

print ('Hit <ENTER> to disconnect\n')
if (sys.version_info > (3, 0)):
    input()
else:
    raw_input()

bleno.stopAdvertising()
bleno.disconnect()

print ('terminated.')
sys.exit(1)
