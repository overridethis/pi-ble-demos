#!/usr/bin/python3
from pybleno import *
import sys
import signal

bleno = Bleno()
uuid = 'e2c56db5dffb48d2b060d0f5a71096e0'

def onStateChange(state):
    print('on -> stateChange: ' + state)

    if (state == 'poweredOn'):
        bleno.startAdvertisingIBeacon(uuid,0,1,-59)
    else:
        bleno.stopAdvertising()

bleno.on('stateChange', onStateChange)

bleno.start()

print ('Hit <ENTER> to disconnect')

if (sys.version_info > (3, 0)):
    input()
else:
    raw_input()

bleno.stopAdvertising()
bleno.disconnect()

print ('terminated.')
sys.exit(1)