# BluetoothLE on RaspberryPi

Demos that showcase prototyping BluetoothLE IoT devices using a RaspberryPi and SenseHat.

|[..]|Name|Theme|
|---|---|---|
|01|Beacon|Proximity iBeacon|
|02|Read Characteristic|Environmental Sensor|
|03|Write Characteristic|LED Switch|
|04|Notifications|Environmental Switch|

>Tested on a Raspberry Pi 3B running Raspbian Stretch.