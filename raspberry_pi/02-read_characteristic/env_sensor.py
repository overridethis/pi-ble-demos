from sense_hat import SenseHat

hat = SenseHat()

class EnvSensor():
    def __init__(self):
        pass

    def get_data(self):
        data = {}
        data["temperature"] = hat.get_temperature()
        data["humidity"] = hat.get_humidity()
        data["pressure"] = hat.get_pressure()
        return data