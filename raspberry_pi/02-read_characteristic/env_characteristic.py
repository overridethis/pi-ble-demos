#!/usr/bin/python3
from pybleno import Characteristic
import json
from threading import Thread
import time

class EnvCharacteristic(Characteristic):
    def __init__(self, uuid, env):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['read'],
            'value': None
        })
        self._env = env

    def onReadRequest(self, offset, callback):
        callback(Characteristic.RESULT_SUCCESS, self._get_payload())

    def _get_payload(self):
        data = self._env.get_data()
        payload = json.dumps(data).encode('UTF-8')
        return payload