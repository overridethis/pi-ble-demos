﻿using System.Collections.ObjectModel;
using BLESwitch.Networking;
using Xamarin.Forms;

namespace BLESwitch
{
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<string> Devices => (ObservableCollection<string>)this.BindingContext;

        public MainPage()
        {
            InitializeComponent();
            this.BindingContext = new ObservableCollection<string>();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            this.Devices.Clear();
            BLEManager.Scan(deviceName =>
            {
                Devices.Add(deviceName);
            });
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            Configuration.DeviceName = (string)e.Item;
            this.Navigation.PushAsync(new DevicePage());
        }
    }
}
