﻿using System;
namespace BLESwitch
{
    public class EnvironmentModel
    {
        public double Temperature { get; set; }
        public double Humidity { get; set; }
        public double Pressure { get; set; }
    }
}
