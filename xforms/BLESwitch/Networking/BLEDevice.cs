﻿using System;
using System.Text;
using System.Threading.Tasks;
using nexus.protocols.ble;

namespace BLESwitch.Networking
{
    public class BLEDevice
    {
        private IBleGattServerConnection _gattServer;

        public BLEDevice(IBleGattServerConnection gattServer)
        {
            this._gattServer = gattServer;
        }

        public async Task<string> ReadValueAsync(Guid characteristicId)
        {
            // TODO: 03 - Read Characteristic Value.
            var payload = await _gattServer.ReadCharacteristicValue(
                BLEManager.SERVICE_ID,
                characteristicId);
            return Encoding.UTF8.GetString(payload);
        }

        public async Task WriteValueAsync(Guid characteristicId, string payload)
        {
            // TODO: 04 - Write Characteritic Value.
            await _gattServer.WriteCharacteristicValue(
                BLEManager.SERVICE_ID,
                characteristicId,
                Encoding.UTF8.GetBytes(payload));
        }

        public async Task Disconnect()
        {
            // TODO: 05 - Disconnect.
            await _gattServer.Disconnect();
        }
    }
}
