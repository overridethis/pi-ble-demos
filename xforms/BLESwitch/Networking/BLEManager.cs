﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using nexus.core.io;
using nexus.protocols.ble;
using nexus.protocols.ble.scan;

namespace BLESwitch.Networking
{
    public static class BLEManager
    {
        public static readonly Guid SERVICE_ID = new Guid("EC030000-0000-0000-0000-000000000000");
        public static readonly Guid SWITCH_ID = new Guid("EC040000-0000-0000-0000-000000000000");
        public static readonly Guid COLOR_ID = new Guid("EC050000-0000-0000-0000-000000000000");
        public static readonly Guid ENV_ID = new Guid("EC060000-0000-0000-0000-000000000000");


        // TODO: 00 - Property - BleAdapter
        private static IBluetoothLowEnergyAdapter _adapter;
        public static IBluetoothLowEnergyAdapter Adapter
        {
            get
            {
                if (_adapter == null)
                    throw new NullReferenceException("BLE Adapter has not been set.");
                return _adapter;
            }
            set
            {
                _adapter = value;
            }
        }

        // Scan.
        public static void Scan(Action<string> callback)
        {
            // TODO: 01 - No Filter - Scan For Peripherals
            // TODO: 01 - Filter - Scan For Peripherals
            // TODO: 01 - Filter, Remove Dupes - Scan For Peripherals

            var discovered = new List<IBlePeripheral>();
            var filter = new ScanFilter();
            filter.AddAdvertisedService(SERVICE_ID);

            _adapter.ScanForBroadcasts(filter, 
                p =>
                {
                    if (discovered.Contains(p))
                        return;

                    discovered.Add(p);
                    callback(p.Advertisement.DeviceName);
                },
                TimeSpan.FromSeconds(10));
        }

        // TODO: 02 - Connect to Peripheral
        public static async Task<BLEDevice> ConnectToAsync(string deviceName)
        {
            var filter = new ScanFilter();
            filter.SetAdvertisedDeviceName(deviceName);

            var connection = await _adapter.FindAndConnectToDevice(filter, TimeSpan.FromSeconds(10));
            if (connection.IsSuccessful())
            {
                if (connection.GattServer.State == ConnectionState.Connected)
                {
                    return new BLEDevice(connection.GattServer);
                }
            }
            throw new Exception("Unable to connect to GATT server.");
        }
    }
}
