﻿using System.Threading.Tasks;
using BLESwitch.Networking;
using Newtonsoft.Json;
using nexus.protocols.ble;
using Xamarin.Forms;

namespace BLESwitch
{
    public partial class DevicePage : ContentPage
    {
        private BLEDevice _device;

        public DevicePage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Task.Run(DoConnectAsync);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Task.Run(async () =>
            {
                await this._device.Disconnect();
                this._device = null;
            });
        }

        void Handle_Refresh(object sender, System.EventArgs e)
        {
            Task.Run(DoRefreshAsync);
        }

        private async Task DoConnectAsync()
        {
            _device = await BLEManager.ConnectToAsync(Configuration.DeviceName);
        }

        private async Task DoRefreshAsync()
        {
            // env.
            var envRaw = await _device.ReadValueAsync(BLEManager.ENV_ID);
            var env = JsonConvert.DeserializeObject<EnvironmentModel>(envRaw);

            // color.
            var colorRaw = await _device.ReadValueAsync(BLEManager.COLOR_ID);
            var colors = JsonConvert.DeserializeObject<int[]>(colorRaw);

            // state.
            var state = await _device.ReadValueAsync(BLEManager.SWITCH_ID);

            Device
                .BeginInvokeOnMainThread(() =>
                {
                    // env.
                    this.TemperatureLabel.Text = env.Temperature.ToString("N2");
                    this.HumidityLabel.Text = env.Humidity.ToString("N2");
                    this.PressureLabel.Text = env.Pressure.ToString("N2");

                    // color.
                    this.LabelColorR.Text = colors[0].ToString();
                    this.LabelColorG.Text = colors[1].ToString();
                    this.LabelColorB.Text = colors[2].ToString();
                    this.SliderColorR.Value = colors[0];
                    this.SliderColorG.Value = colors[1];
                    this.SliderColorB.Value = colors[2];
                    this.BoxColor.BackgroundColor = Color.FromRgb(colors[0], colors[1], colors[2]);

                    // state.
                    this.PowerSwitch.IsToggled = state == "on";
                });
        }

        void HandleSlider_ValueChanged(object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            var slider = (Slider)sender;
            switch(slider.ClassId)
            {
                case "SliderR":
                    this.LabelColorR.Text = this.SliderColorR.Value.ToString("N0");
                    break;
                case "SliderG":
                    this.LabelColorG.Text = this.SliderColorG.Value.ToString("N0");
                    break;
                case "SliderB":
                    this.LabelColorB.Text = this.SliderColorB.Value.ToString("N0");
                    break;
            }
            this.BoxColor.BackgroundColor = Color.FromRgb(this.SliderColorR.Value, this.SliderColorG.Value, this.SliderColorB.Value);
        }

        void Handle_Toggled(object sender, Xamarin.Forms.ToggledEventArgs e)
        {
            Task.Run(async () =>
            {
                await this._device.WriteValueAsync(
                   BLEManager.SWITCH_ID,
                   e.Value ? "on" : "off");
            });
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            var colors = new[] 
            {   
                (int)this.SliderColorR.Value,
                (int)this.SliderColorG.Value,
                (int)this.SliderColorB.Value 
            };
            var j = JsonConvert.SerializeObject(colors);

            Task.Run(async () =>
            {
                await this._device.WriteValueAsync(
                    BLEManager.COLOR_ID,
                    j);
            });
        }
    }
}
